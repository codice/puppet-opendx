# puppet-opendx

A Puppet module for deploying a basic/raw OpenDX node.

---

**Currently only supports initial deployment, and no subsequent Feature
action.  It is possible to extend this module with the provisioning of
the various configuration files required by the underlying Karaf and DDF.**

Once deployed, OpenDX access is available via SSH to port 8101 on the remote node.  User credentials are specified in the remote ${ddf_home}/etc/users.properties file.

---

I happen to be using this with [Vagrant](http://vagrantup.com) to rapidly provision a basic Ubuntu server with a OpenDX environment.

Requires Oracle Java: https://github.com/codice/ddf

I'm using this module to get 'er installed:

http://forge.puppetlabs.com/7terminals/java

Just need to download from Oracle drop it in the modules/java/files subfolder of that java module and add to your site configuration.

For CentOS, this is my site configuration:

```
java::setup {'jdk-7u21-linux-x64':
  source => 'jdk-7u21-linux-x64.tar.gz',
  deploymentdir => '/usr/lib64/jvm/oracle-jdk7',
  user => 'root',
  pathfile => '/etc/profile.d/java.sh',
  cachedir => "/tmp/java-setup-${name}"
}
```

My manifest is super simple:

```
group { "puppet":
  ensure => "present",
}

Exec {
  path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
}

class { "opendx": 
  version => "opendx-standard", 
  package => "1.0"
}
```