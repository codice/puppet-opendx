class opendx($package = "opendx-standard",
	  $version = "1.0-20130430.182740-3"){

  # service { "iptables": ensure => false, enable => false }
  # 
  # case $operatingsystem {
  #   centos: {
  #     package{ "java": name => "java-1.6.0-openjdk", ensure => installed }
  #     $java_home = "/usr/lib/jvm/jre-1.6.0-openjdk.x86_64"
  #   }
  #   ubuntu: { 
  #     exec { "apt-get update": } ->
  #     package{ "java": name => "openjdk-6-jre-headless", ensure => installed }
  #     $java_home = "/usr/lib/jvm/java-6-openjdk-amd64"
  #   }
  # }

	service { "opendx":
		ensure => running,
		enable => true,
		hasstatus => true,
		hasrestart => true,
		subscribe => File["/usr/local/${package}-${version}/etc/OPENDX-wrapper.conf"],
		require => [	File["/usr/local/${package}-${version}/bin/OPENDX-wrapper"],
				File["/usr/local/${package}-${version}/etc/OPENDX-wrapper.conf"],
        File["/etc/init.d/opendx"]]
	}

	# Ensure system dependencies are installed
	package{ "unzip": ensure => installed }

	exec { "get_opendx":
		cwd => "/tmp",
		command => "wget https://tools.codice.org/artifacts/content/repositories/releases/org/codice/opendx/distribution/${package}/${version}/${package}-${version}.zip --no-check-certificate",
		creates => "/tmp/${package}-${version}.zip",
		timeout => 3600,
  } 
		
	if $package == 'opendx-enterprise' {
	# Unpack the OPENDX distribution
		exec { "unzip_enterprise":
      command => "unzip /tmp/${version}/${package}-${version}.zip",
			cwd => "/usr/local",
			creates => "/usr/local/${package}-${version}",
			require => [Package["unzip"], Exec["get_opendx"]],
		} 
	} else {
		exec { "unzip":
			command => "unzip /tmp/${package}-${version}.zip; mv opendx-${version} ${package}-${version}",
			cwd => "/usr/local",
			creates => "/usr/local/${package}-${version}",
			require => [Package["unzip"], Exec["get_opendx"]],
      notify => File["/usr/local/${package}-${version}"]
		}
  }
  
  file { "/usr/local/${package}-${version}":
    ensure => directory
  }
  
	# Setup the system service
	file { "/etc/init.d/opendx":
		notify => Service["opendx"],
		content => template("opendx/opendx.erb"),
		require => File["/usr/local/${package}-${version}/etc/startup.properties"],
		mode => 755,
	}
	file { "/usr/local/${package}-${version}/lib/libwrapper.so":
		source => "puppet:///modules/opendx/libwrapper.so",
		mode => 644,
    require => File["/usr/local/${package}-${version}"]
	}
	file { "/usr/local/${package}-${version}/lib/karaf-wrapper.jar":
		source => "puppet:///modules/opendx/karaf-wrapper.jar",
		mode => 644,
    require => File["/usr/local/${package}-${version}"]
	}
	file { "/usr/local/${package}-${version}/lib/karaf-wrapper-main.jar":
		source => "puppet:///modules/opendx/karaf-wrapper-main.jar",
		mode => 644,
    require => File["/usr/local/${package}-${version}"]
	}

	# Setup the appropriate wrapper
	if $architecture == 'x86_64' {  
		file { "/usr/local/${package}-${version}/bin/OPENDX-wrapper":
			source => "puppet:///modules/opendx/OPENDX-wrapper",
			mode => 755,
      require => File["/usr/local/${package}-${version}"]
		} 
	} else {
		file { "/usr/local/${package}-${version}/bin/OPENDX-wrapper":
			source => "puppet:///modules/opendx/OPENDX-wrapper-32",
			mode => 755,
      require => File["/usr/local/${package}-${version}"]
		} 
	}
	file { "/usr/local/${package}-${version}/etc/OPENDX-wrapper.conf":
		mode => 644,
		content => template("opendx/OPENDX-wrapper.conf.erb"),
		require => [File["/etc/init.d/opendx"],File["/usr/local/${package}-${version}"]]
	}
	file { "/usr/local/${package}-${version}/etc/startup.properties":
		mode => 644,
		source => "puppet:///modules/opendx/startup.properties",
    require => File["/usr/local/${package}-${version}"]
	}

}

